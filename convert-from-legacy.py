#!/usr/bin/env python
from __future__ import annotations

import argparse
import json
import os
from collections import defaultdict
from pathlib import Path

import diraccfg
import yaml

from diracx.core.config import DEFAULT_CONFIG_FILE, Config


def _git_path(value: str) -> Path:
    repo = Path(value)
    if not (repo / ".git").is_dir():
        raise ValueError(f"{repo} does not appear to be a git repository")
    return repo

def _list_to_str(original: str) -> list[str]:
    return [x.strip() for x in original.split(",") if x.strip()]


def parse_args():
    parser = argparse.ArgumentParser("Convert the legacy DIRAC CS to the new format")
    parser.add_argument("old_file", type=Path)
    parser.add_argument("repo", type=_git_path)
    args = parser.parse_args()

    if not os.environ.get("DIRAC_COMPAT_ENABLE_CS_CONVERSION"):
        raise RuntimeError("DIRAC_COMPAT_ENABLE_CS_CONVERSION must be set for the conversion to be possible")

    main(args.old_file, args.repo / DEFAULT_CONFIG_FILE)


def main(old_file: Path, new_file: Path):
    """Load the old CS and convert it to the new YAML formaqt"""
    old_data = old_file.read_text()
    cfg = diraccfg.CFG().loadFromBuffer(old_data)
    raw = cfg.getAsDict()

    apply_fixes(raw)

    config = Config.parse_obj(raw)
    new_data = json.loads(config.json(exclude_unset=True))
    new_file.write_text(yaml.safe_dump(new_data, line_break=False))
    

def apply_fixes(raw):
    """Modify raw in place to make any layout changes between the old and new structure"""
    # Remove dips specific parts from the CS
    raw["DIRAC"].pop("Extensions", None)
    raw["DIRAC"].pop("Framework", None)
    raw["DIRAC"].pop("Security", None)

    # This is VOMS specific and no longer reqired
    raw["DIRAC"].pop("ConnConf", None)

    # Setups are no longer supported
    raw["DIRAC"].pop("DefaultSetup", None)
    raw["DIRAC"].pop("Setups", None)
    raw["DIRAC"].pop("Configuration", None)

    # All installations are no multi-VO
    raw["DIRAC"].pop("VirtualOrganization", None)

    # The default group now lives in /Registry
    raw["DIRAC"].pop("DefaultGroup",None)

    # Modify the registry to be fully multi-VO
    original_registry = raw.pop("Registry")
    raw["Registry"] = {}
    for vo, vo_meta in CONFIG["VOs"].items():
        raw["Registry"][vo] = {
            "IdP": vo_meta["IdP"],
            "DefaultGroup": vo_meta["DefaultGroup"],
            "Users": {},
            "Groups": {},
        }
        if "DefaultStorageQuota" in original_registry:
            raw["Registry"][vo]["DefaultStorageQuota"] = original_registry["DefaultStorageQuota"]
        if "DefaultProxyLifeTime" in original_registry:
            raw["Registry"][vo]["DefaultProxyLifeTime"] = original_registry["DefaultProxyLifeTime"]
        # Find the groups that belong to this VO
        vo_users = set()
        for name, info in original_registry["Groups"].items():
            if info.get("VO", None) == vo:
                raw["Registry"][vo]["Groups"][name] = {
                    k: v for k, v in info.items() if k not in {"IdPRole", "VO"}
                }
                nicknames = {u.strip() for u in info["Users"].split(",") if u.strip()}
                vo_users |= nicknames
                raw["Registry"][vo]["Groups"][name]["Users"] = [vo_meta["UserSubjects"][n] for n in nicknames if n in vo_meta["UserSubjects"]]
        # Find the users that belong to this VO
        for name, info in original_registry["Users"].items():
            if name in vo_users:
                if subject := vo_meta["UserSubjects"].get(name):
                    raw["Registry"][vo]["Users"][subject] = info | {"PreferedUsername": name}
                    raw["Registry"][vo]["Users"][subject]["DN"] = [
                        dn.strip()
                        for dn in raw["Registry"][vo]["Users"][subject]["DN"].split(",")
                        if dn.strip() and not dn.strip().startswith("/O=DIRAC/")
                    ]


# For DIRAC Certification
CONFIG = {
    "VOs": {
        # "dteam": {
        #     "DefaultGroup": "dteam_user",
        #     "IdP": {
        #         "URL": "https://wlcg.cloud.cnaf.infn.it/",
        #         "ClientID": "ba668026-327e-48ba-bc76-e26aa8a18e34",
        #     },
        #     "UserSubjects": {
        #         "cburr": "6ebbcc29-8680-4347-92fb-12c3d37d1e4b",
        #     }
        # }
        "vo": {
            "DefaultGroup": "dirac_user",
            "IdP": {
                "URL": "https://something.invalid/",
                "ClientID": "995ed3b9-d5bd-49d3-a7f4-7fc7dbd5a0cd",
            },
            "UserSubjects": {
                "adminusername": "e2cb28ec-1a1e-40ee-a56d-d899b79879ce",
                "ciuser": "26dbe36e-cf5c-4c52-a834-29a1c904ef74",
                "trialUser": "a95ab678-3fa4-41b9-b863-fe62ce8064ce",
            }
        }
    }
}

# For LHCb Production
# CONFIG = {
#     "VOs": {
#         "lhcb": {
#             "DefaultGroup": "lhcb_user",
#             "IdP": {
#                 "URL": "https://lhcb-auth.web.cern.ch",
#                 "ClientID": "d396912e-2f04-439b-8ae7-d8c585a34790",
#             },
#             "UserSubjects": {
#                 "chaen": "b824d4dc-1f9d-4ee8-8df5-c0ae55d46041",
#                 "cburr": "59382c3f-181c-4df8-981d-ec3e9015fcb9",
#             }
#         }
#     }
# }


if __name__ == "__main__":
    parse_args()
